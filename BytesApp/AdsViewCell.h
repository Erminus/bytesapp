//
//  AdsViewCell.h
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdsViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *adsImage;
@property (weak, nonatomic) IBOutlet UILabel *adsTitle;

@end
