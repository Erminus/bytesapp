//
//  CustomCollectionViewFlowLayout.m
//  BytesApp
//
//  Created by Ermin on 2016-04-25.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "CustomCollectionFlowLayout.h"

@implementation CustomCollectionFlowLayout

-(instancetype)init{
    self = [super init];
    if (self) {
        self.minimumLineSpacing = 1.0;
        self.minimumInteritemSpacing = 1.0;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return self;
}
-(CGSize)itemSize{
    NSInteger numberOfColums = 2;
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame)- (numberOfColums -1 )) / numberOfColums;
    return CGSizeMake(itemWidth, itemWidth);
}

@end
