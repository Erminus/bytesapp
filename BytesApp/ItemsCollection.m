//
//  ItemsCollection.m
//  BytesApp
//
//  Created by Ermin on 2016-04-15.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "ItemsCollection.h"

@interface ItemsCollection()

@property(nonatomic)NSInteger itemId; // Gives an Id to inserted item

@end

@implementation ItemsCollection

-(NSMutableArray *)itemList{
    if (!_itemList) {
        _itemList = [[NSMutableArray alloc]init];
    }
    return _itemList;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        self.itemList = [self loadItems];
    }
    return self;
}

// Adding whole content of one item
-(void)addItems:(NSString *)title
 andDescription:(NSString *)description
   andWithImage:(NSString *)image{
    static int itemCounter = 1;
    self.itemId = itemCounter;
    NSDictionary *itemInfo = @{@"ItemID":[NSNumber numberWithInt:(int)self.itemId],
                               @"title":title,
                               @"description":description,
                               @"image":image};
    [self.itemList addObject:itemInfo];
    itemCounter++;
    [self storeItems];
}



-(void)storeItems {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.itemList forKey:@"items"];
    [defaults synchronize];
}

-(NSMutableArray *)loadItems{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return self.itemList = ((NSArray*)[defaults objectForKey:@"items"]).mutableCopy;
}




@end
