//
//  AboutViewController.h
//  BytesApp
//
//  Created by Ermin on 2016-04-09.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 

@interface AboutViewController : UIViewController<MFMailComposeViewControllerDelegate>

@end
