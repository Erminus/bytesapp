//
//  User.m
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "User.h"

@implementation User

-(instancetype)initWithName:(NSString *)name
               withPassword:(NSString *)password
               andWithEmail:(NSString *)email{
    self = [super init];
    if (self) {
        self.userName = name;
        self.userPassword = password;
        self.userEmail = email;
    }
    return self;
}

@end
