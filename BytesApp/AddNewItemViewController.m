//
//  AddNewItemViewController.m
//  BytesApp
//
//  Created by Ermin on 2016-04-14.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AddNewItemViewController.h"
#import "AdsItems.h"
#import "ItemsCollection.h"
#import "UserDataBase.h"


@interface AddNewItemViewController ()

@property (weak, nonatomic) IBOutlet UITextField *titleText;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIImageView *showImage;
@property(nonatomic)NSString *imageDataPath; // image local directory path
@property(nonatomic)NSString *ImageName;

@property (nonatomic)ItemsCollection *inventory;
@property(nonatomic)UserDataBase *adsUsers;

@property(nonatomic)NSString *user;
@property(nonatomic)NSString *email;
@property(nonatomic)NSString *password;

@end

@implementation AddNewItemViewController

-(UserDataBase *)adsUsers{
    if (!_adsUsers) {
        _adsUsers = [[UserDataBase alloc]init];
    }
    return _adsUsers;
}

-(ItemsCollection *)inventory{
    if (!_inventory) {
        _inventory = [[ItemsCollection alloc]init];
    }
    return _inventory;
}

- (IBAction)addNewItem:(id)sender {
    // Handle empty textfield
    if ([self.titleText.text isEqualToString:@""] || [self.descriptionText.text isEqualToString:@""]) {
        [self alertMessage:@"You have to fill all textfields"];
    }else{
        [self appendNewItem];
        [self.tabBarController setSelectedIndex:0];
    }
}
// Adding new item
-(void)appendNewItem{
    AdsItems *addedItem =[[AdsItems alloc]initWithTitle:self.titleText.text
                                     withAdsDescription:self.descriptionText.text
                                           andWithImage:self.ImageName];
    if (!addedItem.adsImage) {
        addedItem.adsImage = @"No image";
    }
    [self.inventory addItems:addedItem.title andDescription:addedItem.adsDescription andWithImage:addedItem.adsImage];
}
// dismiss the keyboard
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [[self view] endEditing:YES];
}
// Take photo action button
- (IBAction)takePhoto:(id)sender {
    [self takeAdsPhoto];
}

#pragma mark - ImagePicker
-(void)takeAdsPhoto{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    // if device has a camera take a picture else take from library
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.showImage.image = image;
    // catching whole image path
    self.imageDataPath = [self imagePath];
    self.ImageName = [NSString stringWithFormat:@"%@",[self.imageDataPath substringFromIndex:self.imageDataPath.length - 12]];// saving image name from local path
    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL success = [imageData writeToFile:self.imageDataPath atomically:YES];
    if (success) {
        NSLog(@"Saved image to user documents directory");
    } else {
        [self alertMessage:@"Failed to save image"];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
// Return local storage path
-(NSString *)imagePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    NSUUID *uuid = [[NSUUID alloc]init]; // Gives name to image
    NSString *key = [uuid UUIDString];
    NSString *keyPath = [NSString stringWithFormat:@"%@.png",[key substringToIndex:8]];
    return [path stringByAppendingPathComponent:keyPath];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AlertView
// Helps to check if any alert textfield are empty TODO
-(BOOL)checkIfTexfieldsIsValid:(NSString *)user
                   andPassword:(NSString *)password
                    inDataBase:(NSArray *)array{
    for (NSDictionary *dict in array) {
        if (
            (![user isEqualToString:[dict objectForKey:@"userName"]] || [user isEqualToString:@""]) ||
            (![password isEqualToString:[dict objectForKey:@"password"]] || [password isEqualToString:@""])) {
            return YES;
        }else{
            return NO;
        }
    }
    return NO;
}
// Default alertview with message
-(void)alertMessage:(NSString *)message{
    UIAlertController *messageAlert = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    [messageAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:messageAlert animated:YES completion:nil];
    });
}
// Sign up alert
-(void)alertSignUp{
    UIAlertController *signUpAlert = [UIAlertController alertControllerWithTitle:@"Sign up" message:@"To sign up enter username, email and password" preferredStyle:UIAlertControllerStyleAlert];
    
    [signUpAlert addAction:[UIAlertAction actionWithTitle:@"Sign Up" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([signUpAlert.textFields[0].text isEqualToString:@""]||
            [signUpAlert.textFields[1].text isEqualToString:@""]||
            [signUpAlert.textFields[2].text isEqualToString:@""]) {
            [self alertSignUp];
        }else {
            self.user = signUpAlert.textFields[0].text;
            self.email = signUpAlert.textFields[1].text;
            self.password = signUpAlert.textFields[2].text;
            [self.adsUsers addUser:self.user withUserEmail:self.email andWithPassword:self.password];
            [self alertLogin];
        }
    }]];
    [signUpAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Cancel sign up");
        // if user cancel sign up sending to firstview controller
        [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[0]];
    }]];
    // texfield Username
    [signUpAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"User name";
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    // texfield Email
    [signUpAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Email";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    }];
    // textfield Password
    [signUpAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:signUpAlert animated:YES completion:nil];
    });
}
// Alert login
-(void)alertLogin{
    
    UIAlertController *loginAlert = [UIAlertController alertControllerWithTitle:@"Login" message:@"You must login to add item" preferredStyle:UIAlertControllerStyleAlert];
    [loginAlert addAction:[UIAlertAction actionWithTitle:@"Sign in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // Check if all textfields are empty
        if ([self checkIfTexfieldsIsValid:loginAlert.textFields[0].text andPassword:loginAlert.textFields[1].text inDataBase:self.adsUsers.userList]) {
            [self alertLogin];
        }else{
            [self alertMessage:@"Login Success"];
        }
        
        
    }]];
    [loginAlert addAction:[UIAlertAction actionWithTitle:@"Sign Up" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Signing up");
           [self alertSignUp];
    }]];
    [loginAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [loginAlert dismissViewControllerAnimated:YES completion:nil];
        // if user cancel sign in sending to firstview controller
        [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[0]];
    }]];
    // textfield username
    [loginAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Username";
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    // textfield password
    [loginAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:loginAlert animated:YES completion:nil];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self alertLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
