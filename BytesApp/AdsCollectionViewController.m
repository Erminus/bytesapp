//
//  AdsCollectionViewController.m
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AdsCollectionViewController.h"
#import "AdsViewCell.h"
#import "AdsDetailController.h"
#import "ItemsCollection.h"
#import "CustomCollectionFlowLayout.h"


@interface AdsCollectionViewController()
@property(nonatomic)ItemsCollection *items;
@end

@implementation AdsCollectionViewController
-(ItemsCollection *)items{
    if (!_items) {
        _items = [[ItemsCollection alloc]init];
    }
    return _items;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [self.collectionView reloadData];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.collectionView.collectionViewLayout = [[CustomCollectionFlowLayout alloc]init];
}

#pragma mark UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    [self.items loadItems];
    return self.items.itemList.count;
}

-(UIImage *)loadImage:(NSString *)imageName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = paths[0];
    NSString *path = [documentDirectory stringByAppendingPathComponent:[NSString stringWithString:imageName]];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    return image;
}

// Displaying images on cell
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AdsViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [self.items loadItems];

    NSString *path = [self.items.itemList[indexPath.row]objectForKey:@"image"];
    UIImage *image = [self loadImage:path];
    
    if (image) {
        cell.adsImage.image  = image ;
    }else if([[self.items.itemList[indexPath.row]objectForKey:@"image"]isEqualToString:@"No image"]){
        cell.adsImage.image = [UIImage imageNamed:@"default.png"];
    }
    cell.adsTitle.text = [self.items.itemList[indexPath.row] objectForKey:@"title"];
    return cell;
}
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AdsViewCell *cell = sender;
    AdsDetailController *details = segue.destinationViewController;
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    details.itemIndex = (int)indexPath.row; // sending index path to detail view
    details.title = [self.items.itemList[indexPath.row] objectForKey:@"title"];
    //details.detailImage.image = cell.adsImage.image;
    if (![[self.items.itemList[indexPath.row]objectForKey:@"image"]isEqualToString:@"No image"]) {
         details.detailImage.image = cell.adsImage.image;
    } else {
        details.detailImage.image = [UIImage imageNamed:@"default.png"];
    }
    details.detailTitle.text = cell.adsTitle.text;
}

@end
