//
//  AdsItems.h
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdsItems : NSObject

@property(nonatomic)NSString *title; //Item title
@property(nonatomic)NSString *adsImage; //imagefile name
@property(nonatomic)NSString *adsDescription;//item describe

-(instancetype)initWithTitle:(NSString *)title
              withAdsDescription:(NSString *)description
                andWithImage:(NSString *)image;

@end
