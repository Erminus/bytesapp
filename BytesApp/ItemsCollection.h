//
//  ItemsCollection.h
//  BytesApp
//
//  Created by Ermin on 2016-04-15.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemsCollection : NSObject

// dictionary that store all items
@property(nonatomic)NSMutableArray *itemList;

// Adding whole content of one item
-(void) addItems:(NSString *)title
  andDescription:(NSString *)description
    andWithImage:(NSString *)image;

-(void)storeItems;
-(NSMutableArray *)loadItems;


@end
