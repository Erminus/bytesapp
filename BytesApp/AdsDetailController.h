//
//  AdsDetailController.h
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdsDetailController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *detailTitle;
@property (weak, nonatomic) IBOutlet UIImageView *detailImage;
@property (weak, nonatomic) IBOutlet UITextView *detailDescription;
@property (nonatomic)NSUInteger itemIndex;

@end
