//
//  User.h
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic)NSString *userName;
@property(nonatomic)NSString *userPassword;
@property(nonatomic)NSString *userEmail;

-(instancetype)initWithName:(NSString *)name
               withPassword:(NSString *)password
               andWithEmail:(NSString *)email;

@end
