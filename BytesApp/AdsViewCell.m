//
//  AdsViewCell.m
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AdsViewCell.h"

@implementation AdsViewCell

-(void)prepareForReuse{
    [super prepareForReuse];
    self.adsImage.image = nil;
    self.adsTitle.text = nil;
}


@end
