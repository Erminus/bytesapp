//
//  AdsDetailController.m
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AdsDetailController.h"
#import "ItemsCollection.h"

@interface AdsDetailController()

@property (nonatomic)ItemsCollection *itemsDetail;

@end

@implementation AdsDetailController

-(ItemsCollection *)itemsDetail{
    if (!_itemsDetail) {
        _itemsDetail = [[ItemsCollection alloc]init];
    }
    return _itemsDetail;
}
// Loading image from local storage
-(UIImage *)loadImage:(NSString *)imageName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = paths[0];
    NSString *path = [documentDirectory stringByAppendingPathComponent:[NSString stringWithString:imageName]];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    return image;
}
// Display item content
-(void)showItemContent{
    [self.itemsDetail loadItems];
    self.detailTitle.text = [self.itemsDetail.itemList[self.itemIndex]objectForKey:@"title"];
    self.detailDescription.text = [self.itemsDetail.itemList[self.itemIndex]objectForKey:@"description"];
    NSString *path = [self.itemsDetail.itemList[self.itemIndex]objectForKey:@"image"];
    if ([self loadImage:path]) {
        self.detailImage.image = [self loadImage:path];
    } else {
        self.detailImage.image = [UIImage imageNamed:@"default.png"];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self showItemContent];
    [self.itemsDetail loadItems];
   
}


@end
