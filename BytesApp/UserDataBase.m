//
//  UserDataBase.m
//  BytesApp
//
//  Created by Ermin on 2016-04-23.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "UserDataBase.h"
#import "User.h"

@implementation UserDataBase

-(NSMutableArray *)userList{
    if (!_userList) {
        _userList = [[NSMutableArray alloc]init];
    }
    return _userList;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        self.userList = [self getUsers];
    }
    return self;
}

-(void) addUser:(NSString *)user
  withUserEmail:(NSString *)email
andWithPassword:(NSString *)password{
    NSDictionary *users = @{@"userName":user,
                            @"email":email,
                            @"password":password};
    [self.userList addObject:users];
    [self saveUser];
}

-(void)saveUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.userList forKey:@"allUsers"];
    [defaults synchronize];
}

-(NSMutableArray *)getUsers{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return self.userList = ((NSArray *)[defaults objectForKey:@"allUsers"]).mutableCopy;
}

@end
