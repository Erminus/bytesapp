//
//  UserDataBase.h
//  BytesApp
//
//  Created by Ermin on 2016-04-23.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataBase : NSObject

// Array to store all users
@property(nonatomic)NSMutableArray *userList;

// Adding users info
-(void) addUser:(NSString *)user
  withUserEmail:(NSString *)email
andWithPassword:(NSString *)password;

-(void)saveUser;
-(NSMutableArray *)getUsers;

@end
