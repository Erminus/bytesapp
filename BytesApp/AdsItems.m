//
//  AdsItems.m
//  BytesApp
//
//  Created by Ermin on 2016-04-12.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AdsItems.h"

@implementation AdsItems

-(instancetype)initWithTitle:(NSString *)title
          withAdsDescription:(NSString *)description
               andWithImage:(NSString *)image{
    self = [super init];
    if (self) {
        self.title = title;
        self.adsDescription = description;
        self.adsImage = image;

    }
    return self;
}

@end
