//
//  main.m
//  BytesApp
//
//  Created by Ermin on 2016-04-09.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
