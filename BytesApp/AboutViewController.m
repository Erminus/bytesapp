//
//  AboutViewController.m
//  BytesApp
//
//  Created by Ermin on 2016-04-09.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController

// Action to go back to mainView
- (IBAction)goBackToMain:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)contactEmail:(id)sender {
    // Email subject
    NSString *emailTitle = @"Test email";
    // Email content
    NSString *messageBody = @"<h1>Future</h1>"; // Message body to HTML
    // To adress
    NSArray *toRecipents = [NSArray arrayWithObject:@"erminmahmutovic@gmail.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc]init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:YES];
    [mc setToRecipients:toRecipents];
    // Present mail viewcontroller on screen
    [self presentViewController:mc animated:YES completion:nil];
}

// Works only on real ios device--
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@",[error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the mail interface
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
